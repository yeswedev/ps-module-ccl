<?php

class CCLForm
{
    private $table = _DB_PREFIX_ . 'ccl';
    private $cmsId;

    public function __construct($cmsId)
    {
        $this->cmsId = $cmsId;
    }

    public function getBlockIds()
    {
        $sql = 'SELECT DISTINCT id_block
                FROM ' . $this->table . '
                WHERE id_cms =' . $this->cmsId;

        return Db::getInstance()->executeS($sql);
    }

    public function getBlockFields($blockId, $langId)
    {
        $sql = 'SELECT name, value
                FROM ' . $this->table . '
                WHERE id_cms =' . $this->cmsId . '
                AND (name = "CCL_PATTERN_' . $blockId . '" OR name LIKE "CCL_%_' . $blockId . '_' . $langId . '")';

        $rows = Db::getInstance()->executeS($sql);

        $fields = [];
        foreach ($rows as $field) {
            if ($field['name'] != 'CCL_PATTERN_' . $blockId) {
                $fields['values'][] = $field['value'];
            } else {
                $fields['patternId'] = $field['value'];
            }
        }

        return $fields;
    }

    public function getBlockPatternsId($blockId)
    {
        $sql = 'SELECT value
                FROM ' . $this->table . '
                WHERE id_cms = ' . $this->cmsId . '
                AND name = "CCL_PATTERN_' . $blockId . '"
                AND id_block = ' . $blockId;

        return Db::getInstance()->getValue($sql);
    }

    public function getRow($cmsId, $name)
    {
        $sql = 'SELECT *
                FROM ' . $this->table . '
                WHERE id_cms = ' . $cmsId . '
                AND name = "' . $name . '"';

        return Db::getInstance()->getRow($sql);
    }

    public function getValue($name)
    {
        $sql = 'SELECT value
                FROM ' . $this->table . '
                WHERE id_cms = ' . $this->cmsId . '
                AND name = "' . $name .'"';
        return Db::getInstance()->getValue($sql);
    }

    public function updateRow($cmsId, $name, $value, $blockId)
    {
        $data = [
            'id_cms' => $cmsId,
            'name' => $name,
            'value' => Db::getInstance()->escape($value, true),
            'id_block' => $blockId,
        ];

        $dbRow = $this->getRow($cmsId, $name);
        if (!$dbRow) {
            return Db::getInstance()->insert('ccl', $data);
        } else {
            return Db::getInstance()->update('ccl', $data, 'id_cms = ' . $cmsId . ' AND name = "' . $name . '"');
        }
    }

    public function deleteBlock($cmsId, $blockId)
    {
        return Db::getInstance()->delete('ccl', 'id_cms = ' . $cmsId . ' AND id_block = ' . $blockId);
    }
}