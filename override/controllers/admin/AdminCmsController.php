<?php

class AdminCmsController extends AdminCmsControllerCore
{
    public function postProcess()
    {
        if (Tools::isSubmit('viewcms') && ($id_cms = (int)Tools::getValue('id_cms'))) {
            /**
             * Begin CCL module save action
             */
            Hook::exec('actionUpdateCCL', array(), Module::getModuleIdByName('ccl'));
            /**
             * End CCL module save action
             */

            AdminController::postProcess();
            if (($cms = new CMS($id_cms, $this->context->language->id)) && Validate::isLoadedObject($cms)) {
                Tools::redirectAdmin(self::$currentIndex . '&id_cms=' . $id_cms . '&conf=4&updatecms&token=' . Tools::getAdminTokenLite('AdminCmsContent') . '&url_preview=1');
            }
        } elseif (Tools::isSubmit('deletecms')) {
            if (Tools::getValue('id_cms') == Configuration::get('PS_CONDITIONS_CMS_ID')) {
                Configuration::updateValue('PS_CONDITIONS', 0);
                Configuration::updateValue('PS_CONDITIONS_CMS_ID', 0);
            }
            $cms = new CMS((int)Tools::getValue('id_cms'));
            $cms->cleanPositions($cms->id_cms_category);
            if (!$cms->delete()) {
                $this->errors[] = $this->trans('An error occurred while deleting the object.', array(), 'Admin.Notifications.Error')
                    . ' <b>' . $this->table . ' (' . Db::getInstance()->getMsgError() . ')</b>';
            } else {
                Tools::redirectAdmin(self::$currentIndex . '&id_cms_category=' . $cms->id_cms_category . '&conf=1&token=' . Tools::getAdminTokenLite('AdminCmsContent'));
            }
        } elseif (Tools::getValue('submitDel' . $this->table)) {
            // Delete multiple objects
            if ($this->access('delete')) {
                if (Tools::isSubmit($this->table . 'Box')) {
                    $cms = new CMS();
                    $result = true;
                    $result = $cms->deleteSelection(Tools::getValue($this->table . 'Box'));
                    if ($result) {
                        $cms->cleanPositions((int)Tools::getValue('id_cms_category'));
                        $token = Tools::getAdminTokenLite('AdminCmsContent');
                        Tools::redirectAdmin(self::$currentIndex . '&conf=2&token=' . $token . '&id_cms_category=' . (int)Tools::getValue('id_cms_category'));
                    }
                    $this->errors[] = $this->trans('An error occurred while deleting this selection.', array(), 'Admin.Notifications.Error');
                } else {
                    $this->errors[] = $this->trans('You must select at least one element to delete.', array(), 'Admin.Notifications.Error');
                }
            } else {
                $this->errors[] = $this->trans('You do not have permission to delete this.', array(), 'Admin.Notifications.Error');
            }
        } elseif (Tools::isSubmit('submitAddcms') || Tools::isSubmit('submitAddcmsAndPreview')) {
            AdminController::validateRules();
            if (count($this->errors)) {
                return false;
            }
            if (!$id_cms = (int)Tools::getValue('id_cms')) {
                $cms = new CMS();
                $this->copyFromPost($cms, 'cms');
                if (!$cms->add()) {
                    $this->errors[] = $this->trans('An error occurred while creating an object.', array(), 'Admin.Notifications.Error') . ' <b>' . $this->table . ' (' . Db::getInstance()->getMsgError() . ')</b>';
                } else {
                    $this->updateAssoShop($cms->id);
                }
            } else {
                $cms = new CMS($id_cms);
                $this->copyFromPost($cms, 'cms');
                if (!$cms->update()) {
                    $this->errors[] = $this->trans('An error occurred while updating an object.', array(), 'Admin.Notifications.Error') . ' <b>' . $this->table . ' (' . Db::getInstance()->getMsgError() . ')</b>';
                } else {
                    $this->updateAssoShop($cms->id);
                }
            }

            /**
             * Begin CCL module save action
             */
            Hook::exec('actionUpdateCCL', array(), Module::getModuleIdByName('ccl'));
            /**
             * End CCL module save action
             */

            if (Tools::isSubmit('view' . $this->table)) {
                Tools::redirectAdmin(self::$currentIndex . '&id_cms=' . $cms->id . '&conf=4&updatecms&token=' . Tools::getAdminTokenLite('AdminCmsContent') . '&url_preview=1');
            } elseif (Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
                Tools::redirectAdmin(self::$currentIndex . '&' . $this->identifier . '=' . $cms->id . '&conf=4&update' . $this->table . '&token=' . Tools::getAdminTokenLite('AdminCmsContent'));
            } else {
                Tools::redirectAdmin(self::$currentIndex . '&id_cms_category=' . $cms->id_cms_category . '&conf=4&token=' . Tools::getAdminTokenLite('AdminCmsContent'));
            }
        } elseif (Tools::isSubmit('way') && Tools::isSubmit('id_cms') && (Tools::isSubmit('position'))) {
            /* @var CMS $object */
            if (!$this->access('edit')) {
                $this->errors[] = $this->trans('You do not have permission to edit this.', array(), 'Admin.Notifications.Error');
            } elseif (!Validate::isLoadedObject($object = $this->loadObject())) {
                $this->errors[] = $this->trans('An error occurred while updating the status for an object.', array(), 'Admin.Notifications.Error')
                    . ' <b>' . $this->table . '</b> ' . $this->trans('(cannot load object)', array(), 'Admin.Notifications.Error');
            } elseif (!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position'))) {
                $this->errors[] = $this->trans('Failed to update the position.', array(), 'Admin.Notifications.Error');
            } else {
                Tools::redirectAdmin(self::$currentIndex . '&' . $this->table . 'Orderby=position&' . $this->table . 'Orderway=asc&conf=4&id_cms_category=' . (int)$object->id_cms_category . '&token=' . Tools::getAdminTokenLite('AdminCmsContent'));
            }
        } elseif (Tools::isSubmit('statuscms') && Tools::isSubmit($this->identifier)) {
            // Change object status (active, inactive)
            if ($this->access('edit')) {
                if (Validate::isLoadedObject($object = $this->loadObject())) {
                    /** @var CMS $object */
                    if ($object->toggleStatus()) {
                        Tools::redirectAdmin(self::$currentIndex . '&conf=5&id_cms_category=' . (int)$object->id_cms_category . '&token=' . Tools::getValue('token'));
                    } else {
                        $this->errors[] = $this->trans('An error occurred while updating the status.', array(), 'Admin.Notifications.Error');
                    }
                } else {
                    $this->errors[] = $this->trans('An error occurred while updating the status for an object.', array(), 'Admin.Notifications.Error')
                        . ' <b>' . $this->table . '</b> ' . $this->trans('(cannot load object)', array(), 'Admin.Notifications.Error');
                }
            } else {
                $this->errors[] = $this->trans('You do not have permission to edit this.', array(), 'Admin.Notifications.Error');
            }
        } elseif (Tools::isSubmit('submitBulkdeletecms')) {
            // Delete multiple CMS content
            if ($this->access('delete')) {
                $this->action = 'bulkdelete';
                $this->boxes = Tools::getValue($this->table . 'Box');
                if (is_array($this->boxes) && array_key_exists(0, $this->boxes)) {
                    $firstCms = new CMS((int)$this->boxes[0]);
                    $id_cms_category = (int)$firstCms->id_cms_category;
                    if (!$res = AdminController::postProcess(true)) {
                        return $res;
                    }
                    Tools::redirectAdmin(self::$currentIndex . '&conf=2&token=' . Tools::getAdminTokenLite('AdminCmsContent') . '&id_cms_category=' . $id_cms_category);
                }
            } else {
                $this->errors[] = $this->trans('You do not have permission to delete this.', array(), 'Admin.Notifications.Error');
            }
        } else {
            AdminController::postProcess(true);
        }
    }
}