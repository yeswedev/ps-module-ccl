<div class="ccl-front">
    {foreach from=$fields item=field}
        {if $field['patternId'] == 'patterns_text'}
            <div class="p-section p-txt">
                <h2>{$field['values'][0] nofilter}</h2>
            </div>
        {elseif $field['patternId'] == 'patterns_textarea'}
            <div class="p-section p-txtarea">
                {$field['values'][0] nofilter}
            </div>
        {elseif $field['patternId'] == 'patterns_image'}
            <div class="p-section p-img">
                <div class="p-img__image-container">
                    <img src="{$imgDir}{$field['values'][0]}" alt="" class="p-img__image">
                </div>
            </div>
        {elseif $field['patternId'] == 'patterns_textarea_image'}
            <div class="p-section p-txt-img row">
                <div class="p-txt-img__text col-lg-8">
                    {$field['values'][0] nofilter}
                </div>

                <div class="p-txt-img__image-container col-lg-4">
                    <img src="{$imgDir}{$field['values'][1]}" alt="" class="p-txt-img__image">
                </div>
            </div>
        {elseif $field['patternId'] == 'patterns_image_textarea'}
            <div class="p-section p-img-txt row">
                <div class="p-img-txt__image-container col-lg-4">
                    <img src="{$imgDir}{$field['values'][0]}" alt="" class="p-img-txt__image">
                </div>

                <div class="p-img-txt__text col-lg-8">
                    {$field['values'][1] nofilter}
                </div>
            </div>
        {/if}
    {/foreach}
</div>