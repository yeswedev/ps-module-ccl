jQuery(function ($) {
    $(document).ready(function () {
        const cclContainer = $('.ccl');

        // Init form
        $.ajax({
            method: 'GET',
            url: adminControllerLink,
            data: {
                action: 'generateForm',
                cmsId: cmsId,
            },
            dataType: 'json',
            success: function (jsonData) {
                const fields = $(jsonData.fields).find('.ccl-field');

                cclContainer.append(fields);

                initTinyMCE(fields);
            },
        });

        // Add fields
        $(document).on('change', '.ccl-select', function () {
            const select = $(this);
            const selectedPatterns = $('option:selected', this);
            const patternsId = selectedPatterns.val();
            const split = select.attr('id').split('_');
            const blockId = split[split.length - 1];

            $.ajax({
                method: 'GET',
                url: adminControllerLink,
                data: {
                    action: 'generateForm',
                    cmsId: cmsId,
                    add: true,
                    blockId: blockId,
                    patternsId: patternsId,
                },
                dataType: 'json',
                success: function (jsonData) {
                    const fields = $(jsonData.fields).find('.ccl-field');

                    cclContainer.append(fields);
                    select.attr('disabled', true);

                    initTinyMCE(fields);
                },
            });
        });

        initBlockDeletionListener();

        /**
         * Initialize tinyMCE for textarea
         *
         * @param fields
         */
        function initTinyMCE(fields) {
            let tinyMCESelector = '';

            $(fields.find('.rte.autoload_rte')).each(function () {
                tinyMCESelector = tinyMCESelector + '[name="' + $(this).attr('id') + '"],';
            });

            tinySetup({
                selector: tinyMCESelector.slice(0, -1)
            })
        }

        /**
         * Initialize block deletion listener
         */
        function initBlockDeletionListener() {
            $(document).on('click', '.ccl-delete-block', function (e) {
                e.preventDefault();

                const blockId = $(this).closest('.ccl-field').attr('class').split(' ').pop().split('-').pop();

                $('.ccl-block-' + blockId).fadeOut(400, function () {
                    $(this).remove();
                });

                cclContainer.append('<input type="hidden" name="CCL_DELETE_' + blockId + '" value="true">');
            });
        }
    });
});