<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once 'classes/CCLForm.php';

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class CCL extends Module implements WidgetInterface
{
    protected $cmsId;
    protected $defaultLangId;
    protected $templateFile;
    protected $imgDir;
    protected $imgDirFull;

    public function __construct()
    {
        $this->name = 'ccl';
        $this->tab = 'content_management';
        $this->version = '1.1.0';
        $this->author = 'yshorg37';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.7',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Custom CMS layouts');
        $this->description = $this->l('Enables to choose layouts for CMS pages');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->cmsId = Tools::getValue('id_cms');
        $this->defaultLangId = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->templateFile = 'module:ccl/views/templates/front/ccl.tpl';
        $this->imgDir = $this->_path . 'img' . DIRECTORY_SEPARATOR;

        // Will upload image in /module/ccl/img/ even if the module is overrided
        $this->imgDirFull = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if (!parent::install() ||
            !$this->installTab() ||
            !$this->installDb() ||
            !$this->registerHook('displayBackOfficeHeader') ||
            !$this->registerHook('displayAdminCmsContentForm') ||
            !$this->registerHook('actionUpdateCCL') ||
            !$this->registerHook('displayHeader')
        ) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->uninstallDb()
        ) {
            return false;
        }

        return true;
    }

    /**
     * Install an admin tab for the module
     * (active = 0 to hide tab)
     * A module must have a tab to use a ModuleAdminController
     *
     * @return int
     */
    public function installTab()
    {
        $tab = new Tab();
        $tab->active = 0;
        $tab->class_name = 'AdminCCL';
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'Custom CMS Layouts';
        }
        $tab->module = $this->name;
        return $tab->add();
    }

    public function uninstallTab()
    {
        $id_tab = (int)Tab::getIdFromClassName('AdminCCL');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            return $tab->delete();
        }

        return false;
    }

    /**
     * Install CCL table
     *
     * @return bool
     */
    public function installDb()
    {
        $sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'ccl (
                    id INT NOT NULL AUTO_INCREMENT,
                    id_cms INT NOT NULL,
                    name VARCHAR(255) NOT NULL,
                    value TEXT NULL,
                    id_block INT NOT NULL,
                    PRIMARY KEY (id)
                );';

        return Db::getInstance()->execute($sql);
    }

    public function uninstallDb()
    {
        $sql = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ccl;';

        return Db::getInstance()->execute($sql);
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/front.css');
    }

    public function hookDisplayBackOfficeHeader()
    {
        Media::addJsDef(array(
            'cmsId' => $this->cmsId,
            'adminControllerLink' => $this->context->link->getAdminLink('AdminCCL'),
        ));

        if (Tools::getValue('id_cms')) {
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path . 'views/js/admin.js');
        }
    }

    public function hookDisplayAdminCmsContentForm()
    {
        $this->context->smarty->assign([
            'cmsId' => $this->cmsId,
        ]);

        return $this->display(__FILE__, 'views/templates/admin/ccl.tpl');
    }

    /**
     * Called on cms page save:
     * Adds, updates or deletes field to database
     * Move uploaded images to img module directory
     *
     * @return bool|string
     */
    public function hookActionUpdateCCL()
    {
        $CCLForm = new CCLForm($this->cmsId);

        foreach (Tools::getAllValues() as $name => $value) {
            if ($this->isValidField($name, $value)) {
                $splitName = explode('_', $name);
                $blockId = end($splitName);

                // See if a field is translatable or not
                if (count($splitName) > 3) {
                    $blockId = $splitName[2];
                }

                if (!$this->isDeletionField($name)) {
                    if ($this->isImageField($name)
                        && isset($_FILES[$name])
                        && isset($_FILES[$name]['tmp_name'])
                        && !empty($_FILES[$name]['tmp_name'])) {
                        if ($error = ImageManager::validateUpload($_FILES[$name], 4000000)) {
                            return $error;
                        } else {
                            $ext = substr($_FILES[$name]['name'], strrpos($_FILES[$name]['name'], '.') + 1);
                            $file_name = md5($_FILES[$name]['name']) . '.' . $ext;

                            if (!move_uploaded_file($_FILES[$name]['tmp_name'], $this->imgDirFull . $file_name)) {
                                return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                            } else {
                                if ($CCLForm->getValue($name) != $file_name) {
                                    @unlink($this->imgDirFull . $CCLForm->getValue($name));
                                }
                            }

                            $value = $file_name;
                        }
                    }

                    $CCLForm->updateRow($this->cmsId, $name, $value, $blockId);
                } elseif ($CCLForm->getBlockPatternsId($blockId)) {
                    $CCLForm->deleteBlock($this->cmsId, $blockId);
                }
            }
        }
    }

    /**
     * Check if the field name has CCL_ prefix and
     * if the field isn't the default field
     *
     * @param $name
     * @param $value
     * @return bool
     */
    public function isValidField($name, $value)
    {
        if (strpos($name, 'CCL_') !== false && $value != 'patterns_default') {
            return true;
        }
        return false;
    }

    /**
     * Check if the field has image type
     *
     * @param $name
     * @return bool
     */
    public function isImageField($name)
    {
        if (strpos($name, 'IMAGE') !== false) {
            return true;
        }
        return false;
    }

    /**
     * Check if the field is a deletion field
     *
     * @param $name
     * @return bool
     */
    public function isDeletionField($name)
    {
        if (strpos($name, 'DELETE') !== false) {
            return true;
        }
        return false;
    }

    public function renderWidget($hookName, array $params)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('ccl'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('ccl'));
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $fields = [];
        $CCLForm = new CCLForm($this->cmsId);

        $blockIds = $CCLForm->getBlockIds();
        foreach ($blockIds as $blockId) {
            $blockId = $blockId['id_block']; // dumb var name
            $fields[] = $CCLForm->getBlockFields($blockId, $this->context->language->id);
        }
        
        return array(
            'fields' => $fields,
            'imgDir' => $this->imgDir,
        );
    }
}