<?php

require_once(dirname(__FILE__) . '/../../classes/CCLForm.php');

class AdminCCLController extends ModuleAdminController
{
    private $cclModule;
    private $defaultLangId;
    private $languages;

    public function __construct()
    {
        parent::__construct();

        $this->cclModule = Module::getInstanceByName('ccl');
        $this->defaultLangId = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->languages = $this->context->controller->getLanguages();
    }

    /**
     * Returns the generated html fields
     */
    public function ajaxProcessGenerateForm()
    {
        $cmsId = Tools::getValue('cmsId');
        $fields = $this->getFields($cmsId);

        $helper = new HelperForm();
        $helper->module = $this->cclModule;
        $helper->default_form_language = $this->defaultLangId;
        $helper->allow_employee_form_lang = $this->defaultLangId;
        $helper->tpl_vars = [
            'uri' => $this->cclModule->getPathUri(),
            'languages' => $this->languages,
        ];

        $form[0]['form']['input'] = $fields['patterns'];
        $helper->fields_value = $fields['values'];

        return die(json_encode(array(
            'fields' => $helper->generateForm($form)
        )));
    }

    /**
     * Returns fields depending on action
     *
     * if add action, return a clean field
     * else return every stored fields of current cms page from database
     *
     * @param $cmsId
     * @return array
     */
    public function getFields($cmsId)
    {
        $fields = [
            'patterns' => [],
            'values' => []
        ];

        $CCLForm = new CCLForm($cmsId);

        $add = Tools::getValue('add');

        if ($add) {
            $blockId = Tools::getValue('blockId');

            $patternsId = Tools::getValue('patternsId');
            $patterns = $this->getPatterns($blockId)[$patternsId];
            foreach ($patterns as $field) {
                $fields['patterns'][] = $field;
                foreach ($this->languages as $lang) {
                    $fields['values'][$field['name']][$lang['id_lang']] = '';
                }
            }
            $fields['values']['CCL_PATTERN_' . $blockId] = $patternsId;

            $defaultField = $this->getDefaultPattern($blockId + 1);
        } else {
            $blockIds = $CCLForm->getBlockIds();
            foreach ($blockIds as $blockId) {
                $blockId = $blockId['id_block']; // dumb var name

                $patternsId = $CCLForm->getBlockPatternsId($blockId);
                $patterns = $this->getPatterns($blockId)[$patternsId];
                foreach ($patterns as $field) {
                    $fields['patterns'][] = $field;
                    foreach ($this->languages as $lang) {
                        $fields['values'][$field['name']][$lang['id_lang']] = $CCLForm->getValue($field['name'] . '_' . $lang['id_lang']);
                    }
                    $fields['values']['CCL_PATTERN_' . $blockId] = $CCLForm->getValue($field['name']);
                }
            }

            $defaultField = $this->getDefaultPattern(end($blockIds)['id_block'] + 1);
        }
        $fields['patterns'][] = $defaultField;
        $fields['values'][$defaultField['name']] = '';

        return $fields;
    }

    /**
     * @param $blockId
     * @return array
     */
    public function getPatterns($blockId)
    {
        /**
         * You can add new patterns here
         * Name of field MUST follow the syntax CCL_TYPEOFFIELD_BLOCKID
         * /!\ don't forget to update default field in getDefaultPattern function below and in front view /!\
         */
        $patterns = [
            'patterns_text' => [
                [
                    'type' => 'text',
                    'label' => 'Texte simple (Titre séparateur)',
                    'name' => 'CCL_TEXT_' . $blockId,
                ],
            ],
            'patterns_textarea' => [
                [
                    'type' => 'textarea',
                    'label' => 'Zone de texte',
                    'name' => 'CCL_TEXTAREA_' . $blockId,
                    'autoload_rte' => true,
                ],
            ],
            'patterns_image' => [
                [
                    'type' => 'file',
                    'label' => 'Image',
                    'name' => 'CCL_IMAGE_' . $blockId,
                ],
            ],
            'patterns_textarea_image' => [
                [
                    'type' => 'textarea',
                    'label' => 'Zone de texte et image',
                    'name' => 'CCL_TEXTAREA_' . $blockId,
                    'autoload_rte' => true,
                ],
                [
                    'type' => 'file',
                    'name' => 'CCL_IMAGE_' . $blockId,
                ],
            ],
            'patterns_image_textarea' => [
                [
                    'type' => 'file',
                    'label' => 'Image et zone de texte',
                    'name' => 'CCL_IMAGE_' . $blockId,
                ],
                [
                    'type' => 'textarea',
                    'name' => 'CCL_TEXTAREA_' . $blockId,
                    'autoload_rte' => true,
                ],
            ]
        ];

        foreach ($patterns as $patternId => $pattern) {
            foreach ($pattern as $key => $field) {
                $patterns[$patternId][$key]['form_group_class'] = 'ccl-field ccl-block-' . $blockId;
                $patterns[$patternId][0]['delete_button'] = true;
            }

            $patterns[$patternId][] = [
                'type' => 'hidden',
                'label' => '',
                'name' => 'CCL_PATTERN_' . $blockId,
                'form_group_class' => 'ccl-field ccl-block-' . $blockId,
            ];
        }

        return $patterns;
    }

    /**
     *
     * @param $blockId
     * @return array
     */
    public function getDefaultPattern($blockId)
    {
        /**
         * Add new patterns id here
         * /!\ don't forget to update patterns in getBlockPatterns function above and in front view /!\
         */
        return [
            'type' => 'select',
            'label' => 'Ajouter un champs de mise en page',
            'hint' => 'Choisissez parmi différentes mises en page',
            'name' => 'CCL_SELECT_' . $blockId,
            'class' => 'ccl-select',
            'form_group_class' => 'ccl-field ccl-block-' . $blockId,
            'options' => array(
                'query' => [
                    [
                        'patterns_id' => 'patterns_default',
                        'name' => 'Choisir',
                    ],
                    [
                        'patterns_id' => 'patterns_text',
                        'name' => 'Texte simple (Titre séparateur)',
                    ],
                    [
                        'patterns_id' => 'patterns_textarea',
                        'name' => 'Zone de texte',
                    ],
                    [
                        'patterns_id' => 'patterns_image',
                        'name' => 'Image',
                    ],
                    [
                        'patterns_id' => 'patterns_textarea_image',
                        'name' => 'Texte à gauche et image à droite',
                    ],
                    [
                        'patterns_id' => 'patterns_image_textarea',
                        'name' => 'Image à gauche et texte à droite',
                    ],
                ],
                'id' => 'patterns_id',
                'name' => 'name',
            ),
        ];
    }
}